/**
 *  ACCORDION
 *
 * A lightwight vanilla JS accordion with an exstensible API
 */

// import uuid from 'uuid/v4';
// const uuidV4 = uuid;
/* eslint-disable no-unused-vars */
import arrayFromPolyfill from './array-from-polyfill';
import onCSSTransitionEnd from './transition-end';

/**
 * CONSTRUCTOR
 * initialises the object
 */
class Accordion {
    constructor(el, options) {
        const container = typeof el === 'string' ? document.querySelector(el) : el;

        // If el is not defined
        if (container == null) {
            return;
        }


        const defaults = {
            triggerClass:       '.accordion__trigger',
            itemClass:          '.accordion__item',
            panelClass:         '.accordion__panel',
            panelInnerClass:    '.accordion__panel-inner',
            hiddenClass:        'accordion__item--closed',
            activeClass:        'accordion__item--open',
            initalisedClass:    'accordion--initalised',
            itemDataAttr:       'data-accordion-item-id',
            openMultiplePanels: false,
            openItemsOnLoad:  [],
            triggerOpenLabel:    'Open accordion panel',
            triggerCloseLabel:   'Close accordion panel',
            itemLabel:          'Accordion panel',
            roles:              true
            // toggleEl:            // If you want to use a different element to trigger the accordion
        };

        // Options
        this.settings = Object.assign({}, defaults, options);


        // Setting getting elements
        this.container = container;
        this.items = Array.from( this.container.querySelectorAll(this.settings.itemClass) );
        this.triggers = Array.from( this.container.querySelectorAll(this.settings.triggerClass) );
        this.panels = Array.from( this.container.querySelectorAll(this.settings.panelClass) );
        this.toggleEl = this.settings.toggleEl !== undefined ? Array.from(this.container.querySelectorAll(this.settings.toggleEl)) : this.triggers;

        // This is for managing state of the accordion. It by default sets
        // all accordion panels to be closed
        this.states = [].map.call(this.items, () => {
            return { state: 'closed' };
        });

        this.ids = [].map.call(this.items, () => {
            return { id: Math.floor((Math.random() * 1000000) + 1) };
        });

        // This is to ensure that once an open/close event has been fired
        // another cannot start until the first event has finished.
        // @TODO - get this working...
        this.toggling = false;

        // Initiating the accordion
        if( this.container ) {
            this.init();
        } else {
            /* eslint-disable no-console */
            console.log('Something is wrong with you markup...');
        }
    }


    /**
     *  INIT
     *
     *  Initalises the accordion
     */
    init() {
        // Sets up ID, aria attrs & data-attrs
        this._setupAttributes();

        // Setting up the inital view of the accordion
        this._initalState();

        // Setting the height of each panel
        this.calculateAllPanelsHeight();

        // Inserting data-attribute onto each `trigger`
        this._insertDataAttrs();

        // Adding listeners to triggers
        this._addListeners();

        // Adds class to accordion for initalisation
        this._finishInitalisation();
    }

    /**
     * CHECK ROLES ETTING
     * @return {[boolean]}
     * Checks roles setting for all roles or a single role.
     * First checks if a `boolean` has been used to set all
     * roles to either true or false. If the setting is an
     * object it will only set the attribute where each
     * attribute has explicitly been set as true, eg;
     * ```
     * roles: {
     *     region: true
     * }
     * ```
     */
    _setRole(role, el) {
        if(typeof this.settings.roles === 'boolean' && this.settings.roles || this.settings.roles[role] !== undefined && this.settings.roles[role] !== false) {
            el.setAttribute('role', role);
        }
    }


    /**
     *  INSERT DATA ATTRS
     *
     *  Updates state object for inital loading of the accordion
     */
    _initalState() {
        // Sets state object as per `this.settings.openItemsOnLoad`
        const itemsToOpen = this.settings.openItemsOnLoad;

        if (itemsToOpen.length) {
            this._openItemsOnLoad(itemsToOpen);
        }

        // Render DOM as per the updates `this.states` object
        this._renderDom();
    }


    /**
     *  INSERT DATA ATTRS
     *
     *  Adds `itemDataAttr` to all items
     */
    _insertDataAttrs() {
        this.items.forEach( (item, index) => {
            item.setAttribute(this.settings.itemDataAttr, index);
        });
    }


    /**
     *  FINISH INITALISATION
     *
     *  Adds in `initalisedClass` to accordion
     */
    _finishInitalisation() {
        this.container.classList.add(this.settings.initalisedClass);
        this._setRole('presentation', this.container);
    }


    /**
     *  ADD LISTENERS
     *
     *  Adds click event to each trigger
     */
    _addListeners() {
        // So we can reference the badger-accordion object inside out eventListener
        const _this = this;

        // Adding click event to accordion
        this.triggers.forEach((trigger, index) => {
            trigger.addEventListener('click', function() {
                // Getting the target of the click
                // const clickedEl = event.target;

                _this.handleClick(trigger, index);
            });
        });
    }


    /**
     *  HANDLE CLICK
     *
     *  Handles click and checks if click was on an trigger element
     *  @param {object} targetTrigger - The trigger node you want to open
     */
    handleClick(targetTrigger, triggerIndex) {
        // Removing current `.` from `this.settings.triggerClass` class so it can
        // be checked against the `targeTrigger` classList
        const targetTriggerClass = this.settings.triggerClass.substr(1);

        // Checking that the thing that was clicked on was the accordions trigger
        if (targetTrigger.classList.contains(targetTriggerClass) && this.toggling === false) {
            this.toggling = true;

            // Updating states
            this.setState(triggerIndex);


            // Render DOM as per the updates `this.states` object
            this._renderDom();
        }
    }


    /**
     *  SET STATES
     *
     *  Sets the state for all triggers. The 'target trigger' will have its state toggeled
     *  @param {object} targetTriggerId - The trigger node you want to open
     */
    setState(targetTriggerId) {
        const states = this.getState();

        // If `this.settings.openMultiplePanels` is false we need to ensure only one panel
        // be can open at once. If it is false then all panels state APART from the one that
        // has just been clicked needs to be set to 'closed'.
        if (!this.settings.openMultiplePanels) {
            states.filter((state, index) => {
                if (index != targetTriggerId) {
                    state.state = 'closed';
                }
            });
        }

        // Toggles the state value of the target trigger. This was `array.find` but `find`
        // isnt supported in IE11
        states.filter((state, index) => {
            if (index == targetTriggerId) {
                const newState = this.toggleState(state.state);
                return (state.state = newState);
            }
        });
    }


    /**
     *  RENDER DOM
     *
     *  Renders the accordion in the DOM using the `this.states` object
     */
    _renderDom() {
        // const states = this.getState();

        // Filter through all open triggers and open them
        this.states.filter( (state, index) => {
            if(state.state === 'open') {
                this.open(index);
            }
        });

        // Filter through all closed triggers and closes them
        this.states.filter( (state, index) => {
            if(state.state === 'closed') {
                this.close(index);
            }
        });
    }


    /**
     *  OPEN
     *
     *  Closes a specific panel
     *  @param {integer} triggerIndex - The trigger node index you want to open
     */
    open(triggerIndex) {
        this.togglePanel('open', triggerIndex);
    }


    /**
     *  CLOSE
     *
     *  Closes a specific panel
     *  @param {integer} triggerIndex - The trigger node index you want to close
     */
    close(triggerIndex) {
        this.togglePanel('closed', triggerIndex);
    }


    /**
     *  OPEN ALL
     *
     *  Opens all panels
     */
    openAll() {
        this.items.forEach((item, triggerIndex) => {
            this.togglePanel('open', triggerIndex);
        });
    }


    /**
     *  CLOSE ALL
     *
     *  Closes all panels
     */
    closeAll() {
        this.item.forEach((item, triggerIndex) => {
            this.togglePanel('closed', triggerIndex);
        });
    }


    /**
     *  GET STATE
     *
     *  Getting state of triggers. By default gets state of all triggers
     *  @param {string} animationAction - The animation you want to invoke
     *  @param {integer} triggerIndex    - The trigger node index you want to animate
     */
    togglePanel(animationAction, triggerIndex) {
        if(animationAction !== undefined && triggerIndex !== undefined) {
            if(animationAction === 'closed') {
                // 1. Getting ID of panel that we want to close
                const trigger        = this.triggers[triggerIndex];
                const panelToClose  = this.panels[triggerIndex];

                const item = this.items[triggerIndex];

                // 2. Closing panel
                item.classList.add(this.settings.hiddenClass);

                // 3. Removing active classes
                item.classList.remove(this.settings.activeClass);

                // 4. Set aria attrs
                item.setAttribute('aria-expanded', false);
                trigger.setAttribute('aria-label', this.settings.triggerOpenLabel);

                // 5. Resetting toggling so a new event can be fired
                panelToClose.onCSSTransitionEnd(() => this.toggling = false );
            } else if(animationAction === 'open') {
                // 1. Getting ID of panel that we want to open
                const trigger      = this.triggers[triggerIndex];
                const panelToOpen = this.panels[triggerIndex];
                const item = this.items[triggerIndex];

                // 2. Opening panel
                item.classList.remove(this.settings.hiddenClass);

                // 3. Adding active classes
                item.classList.add(this.settings.activeClass);

                // 4. Set aria attrs
                item.setAttribute('aria-expanded', true);
                trigger.setAttribute('aria-label', this.settings.triggerCloseLabel);

                // 5. Resetting toggling so a new event can be fired
                panelToOpen.onCSSTransitionEnd(() => this.toggling = false );
            }
        }
    }


    /**
     *  GET STATE
     *
     *  Getting state of items. By default gets state of all items
     *  @param {array} itemIds - Id/'s of the triggers you want to check
     */
    getState(itemIds = []) {
        if(itemIds.length && Array.isArray(itemIds)) {
            let states = itemIds.map( item => this.states[item] );

            return states;
        } else {
            return this.states;
        }
    }


    /**
     *  TOGGLE STATE
     *
     *  Toggling the state value
     *  @param {string} currentState - Current state value for a iten
     */
    toggleState(currentState) {
        if(currentState !== undefined) {
            return (currentState === 'closed') ? 'open' : 'closed';
        }
    }



    /**
     *  Items TO OPEN
     *
     *  Setting which item should be open when accordion is initalised
     *  @param {array} itemsToOpen - Array of ID's for the item to be open
     */
    _openItemsOnLoad(itemsToOpen = []) {
        if (itemsToOpen.length && Array.isArray(itemsToOpen)) {
            let items = itemsToOpen.filter(item => item != undefined);

            items.forEach(item => {
                return (this.states[item].state = 'open');
            });
        }
    }


    /**
     *  SET UP ATTRIBUTES
     *
     *  Initalises accordion attribute methods
     */
    _setupAttributes() {
        // Adding ID & aria-controls
        this._setupTriggers();

        // Adding ID & aria-labeledby
        this._setupPanels();

        this._setupItems();

        // Inserting data-attribute onto each `trigger`
        this._insertDataAttrs();
    }



    /**
     *  CALCULATE PANEL HEIGHT
     *
     *  Setting height for panels using pannels inner element
     */
    calculatePanelHeight(panel) {
        const panelInner = panel.querySelector(this.settings.panelInnerClass);

        let activeHeight = panelInner.offsetHeight;

        return panel.style.maxHeight = `${activeHeight}px`;
    }



    /**
     *  CALCULATE PANEL HEIGHT
     *
     *  Setting height for panels using pannels inner element
     */
    calculateAllPanelsHeight() {
        this.panels.forEach(panel => {
            this.calculatePanelHeight(panel);
        });
    }



    /**
     * SET UP TRIGGERS
     */
    _setupTriggers() {
        this.triggers.forEach( (trigger, index) => {
            trigger.setAttribute('id', `accordion-trigger-${this.ids[index].id}`);
            trigger.setAttribute('aria-controls', `accordion-panel-${this.ids[index].id}`);
            trigger.setAttribute('aria-label', this.settings.triggerOpenLabel);
        });
    }


    /**
     * SET UP PANELS
     */
    _setupPanels() {
        this.panels.forEach( (panel, index) => {
            panel.setAttribute('id', `accordion-panel-${this.ids[index].id}`);
            panel.setAttribute('aria-labeledby', `accordion-trigger-${this.ids[index].id}`);
            if(this.settings.roles === true || this.settings.roles.region !== false) {
                this._setRole('region', panel);
            }
        });
    }

    /**
     * SET UP ITEMS
     */
    _setupItems() {
        this.items.forEach( (item, index) => {
            item.setAttribute('id', `accordion-item-${this.ids[index].id}`);
            item.setAttribute('aria-label', this.settings.itemLabel);
        });
    }    
}


// Export
export default Accordion;