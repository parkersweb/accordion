import babel from 'rollup-plugin-babel';
import eslint from 'rollup-plugin-eslint';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import includePaths from 'rollup-plugin-includepaths';
import uglify from 'rollup-plugin-uglify-es';
import replace from 'rollup-plugin-replace';
import copy from 'rollup-plugin-copy';

// Default output options
let output = [
    {
        file: (process.env.NODE_ENV === 'production' && 'dist/accordion.min.js' || process.env.NODE_ENV === 'example' && 'example/js/app.js' || 'dist/accordion.js'),
        format: 'umd'
    },
    {
        file: (process.env.NODE_ENV === 'production' && 'dist/accordion.esm.min.js' || 'dist/accordion.esm.js'),
        format: 'es',
    }
];

// Setting output like this to avoid `.esm.js` from having the `/example` code in it
if(process.env.NODE_ENV === 'example') {
    output.splice(1, 1);
}

export default {
    input: ('src/js/accordion.js'),
    sourcemap: 'false',
    name: 'Accordion',
    output: output,
    plugins: [
        resolve(),
        commonjs(),
        eslint({
            exclude: ['src/css/**', 'src/scss/**']
        }),
        babel({exclude: 'node_modules/**'}),
        includePaths(),
        replace({
            ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        }),
        (process.env.NODE_ENV === 'production'),
        (process.env.NODE_ENV === 'production' &&
            copy({
                'src/js/array-from-polyfill.js' : 'dist/array-from-polyfill.js'
            })
        )
    ]
};