(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.Accordion = factory());
}(this, (function () { 'use strict';

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

if (!Array.from) {
  Array.from = function () {
    var toStr = Object.prototype.toString;

    var isCallable = function isCallable(fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };

    var toInteger = function toInteger(value) {
      var number = Number(value);

      if (isNaN(number)) {
        return 0;
      }

      if (number === 0 || !isFinite(number)) {
        return number;
      }

      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };

    var maxSafeInteger = Math.pow(2, 53) - 1;

    var toLength = function toLength(value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    }; // The length property of the from method is 1.


    return function from(arrayLike
    /* , mapFn, thisArg */
    ) {
      // 1. Let C be the this value.
      var C = this; // 2. Let items be ToObject(arrayLike).

      var items = Object(arrayLike); // 3. ReturnIfAbrupt(items).

      if (arrayLike == null) {
        throw new TypeError('Array.from requires an array-like object - not null or undefined');
      } // 4. If mapfn is undefined, then let mapping be false.


      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;

      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        } // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.


        if (arguments.length > 2) {
          T = arguments[2];
        }
      } // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).


      var len = toLength(items.length); // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method
      // of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).

      var A = isCallable(C) ? Object(new C(len)) : new Array(len); // 16. Let k be 0.

      var k = 0; // 17. Repeat, while k < len… (also steps a - h)

      var kValue;

      while (k < len) {
        kValue = items[k];

        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }

        k += 1;
      } // 18. Let putStatus be Put(A, "length", len, true).


      A.length = len; // 20. Return A.

      return A;
    };
  }();
}

/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

/* eslint-disable no-unused-vars */
(function (document, window) {
  var el = document.body || document.documentElement,
      s = el.style,
      prefixAnimation = '',
      prefixTransition = '';
  if (s.WebkitAnimation == '') prefixAnimation = '-webkit-';
  if (s.MozAnimation == '') prefixAnimation = '-moz-';
  if (s.OAnimation == '') prefixAnimation = '-o-';
  if (s.WebkitTransition == '') prefixTransition = '-webkit-';
  if (s.MozTransition == '') prefixTransition = '-moz-';
  if (s.OTransition == '') prefixTransition = '-o-';
  Object.defineProperty(Object.prototype, 'onCSSAnimationEnd', {
    value: function value(callback) {
      var runOnce = function runOnce(e) {
        callback();
        e.target.removeEventListener(e.type, runOnce);
      };

      this.addEventListener('webkitAnimationEnd', runOnce);
      this.addEventListener('mozAnimationEnd', runOnce);
      this.addEventListener('oAnimationEnd', runOnce);
      this.addEventListener('oanimationend', runOnce);
      this.addEventListener('animationend', runOnce);
      if (prefixAnimation == '' && !('animation' in s) || getComputedStyle(this)[prefixAnimation + 'animation-duration'] == '0s') callback();
      return this;
    },
    enumerable: false,
    writable: true
  });
  Object.defineProperty(Object.prototype, 'onCSSTransitionEnd', {
    value: function value(callback) {
      var runOnce = function runOnce(e) {
        callback();
        e.target.removeEventListener(e.type, runOnce);
      };

      this.addEventListener('webkitTransitionEnd', runOnce);
      this.addEventListener('mozTransitionEnd', runOnce);
      this.addEventListener('oTransitionEnd', runOnce);
      this.addEventListener('transitionend', runOnce);
      this.addEventListener('transitionend', runOnce);
      if (prefixTransition == '' && !('transition' in s) || getComputedStyle(this)[prefixTransition + 'transition-duration'] == '0s') callback();
      return this;
    },
    enumerable: false,
    writable: true
  });
})(document, window, 0);

/**
 *  ACCORDION
 *
 * A lightwight vanilla JS accordion with an exstensible API
 */
// import uuid from 'uuid/v4';
// const uuidV4 = uuid;

/* eslint-disable no-unused-vars */
/**
 * CONSTRUCTOR
 * initialises the object
 */

var Accordion =
/*#__PURE__*/
function () {
  function Accordion(el, options) {
    _classCallCheck(this, Accordion);

    var container = typeof el === 'string' ? document.querySelector(el) : el; // If el is not defined

    if (container == null) {
      return;
    }

    var defaults = {
      triggerClass: '.accordion__trigger',
      itemClass: '.accordion__item',
      panelClass: '.accordion__panel',
      panelInnerClass: '.accordion__panel-inner',
      hiddenClass: 'accordion__item--closed',
      activeClass: 'accordion__item--open',
      initalisedClass: 'accordion--initalised',
      itemDataAttr: 'data-accordion-item-id',
      openMultiplePanels: false,
      openItemsOnLoad: [],
      triggerOpenLabel: 'Open accordion panel',
      triggerCloseLabel: 'Close accordion panel',
      itemLabel: 'Accordion panel',
      roles: true // toggleEl:            // If you want to use a different element to trigger the accordion

    }; // Options

    this.settings = _extends({}, defaults, options); // Setting getting elements

    this.container = container;
    this.items = Array.from(this.container.querySelectorAll(this.settings.itemClass));
    this.triggers = Array.from(this.container.querySelectorAll(this.settings.triggerClass));
    this.panels = Array.from(this.container.querySelectorAll(this.settings.panelClass));
    this.toggleEl = this.settings.toggleEl !== undefined ? Array.from(this.container.querySelectorAll(this.settings.toggleEl)) : this.triggers; // This is for managing state of the accordion. It by default sets
    // all accordion panels to be closed

    this.states = [].map.call(this.items, function () {
      return {
        state: 'closed'
      };
    });
    this.ids = [].map.call(this.items, function () {
      return {
        id: Math.floor(Math.random() * 1000000 + 1)
      };
    }); // This is to ensure that once an open/close event has been fired
    // another cannot start until the first event has finished.
    // @TODO - get this working...

    this.toggling = false; // Initiating the accordion

    if (this.container) {
      this.init();
    } else {
      /* eslint-disable no-console */
      console.log('Something is wrong with you markup...');
    }
  }
  /**
   *  INIT
   *
   *  Initalises the accordion
   */


  _createClass(Accordion, [{
    key: "init",
    value: function init() {
      // Sets up ID, aria attrs & data-attrs
      this._setupAttributes(); // Setting up the inital view of the accordion


      this._initalState(); // Setting the height of each panel


      this.calculateAllPanelsHeight(); // Inserting data-attribute onto each `trigger`

      this._insertDataAttrs(); // Adding listeners to triggers


      this._addListeners(); // Adds class to accordion for initalisation


      this._finishInitalisation();
    }
    /**
     * CHECK ROLES ETTING
     * @return {[boolean]}
     * Checks roles setting for all roles or a single role.
     * First checks if a `boolean` has been used to set all
     * roles to either true or false. If the setting is an
     * object it will only set the attribute where each
     * attribute has explicitly been set as true, eg;
     * ```
     * roles: {
     *     region: true
     * }
     * ```
     */

  }, {
    key: "_setRole",
    value: function _setRole(role, el) {
      if (typeof this.settings.roles === 'boolean' && this.settings.roles || this.settings.roles[role] !== undefined && this.settings.roles[role] !== false) {
        el.setAttribute('role', role);
      }
    }
    /**
     *  INSERT DATA ATTRS
     *
     *  Updates state object for inital loading of the accordion
     */

  }, {
    key: "_initalState",
    value: function _initalState() {
      // Sets state object as per `this.settings.openItemsOnLoad`
      var itemsToOpen = this.settings.openItemsOnLoad;

      if (itemsToOpen.length) {
        this._openItemsOnLoad(itemsToOpen);
      } // Render DOM as per the updates `this.states` object


      this._renderDom();
    }
    /**
     *  INSERT DATA ATTRS
     *
     *  Adds `itemDataAttr` to all items
     */

  }, {
    key: "_insertDataAttrs",
    value: function _insertDataAttrs() {
      var _this2 = this;

      this.items.forEach(function (item, index) {
        item.setAttribute(_this2.settings.itemDataAttr, index);
      });
    }
    /**
     *  FINISH INITALISATION
     *
     *  Adds in `initalisedClass` to accordion
     */

  }, {
    key: "_finishInitalisation",
    value: function _finishInitalisation() {
      this.container.classList.add(this.settings.initalisedClass);

      this._setRole('presentation', this.container);
    }
    /**
     *  ADD LISTENERS
     *
     *  Adds click event to each trigger
     */

  }, {
    key: "_addListeners",
    value: function _addListeners() {
      // So we can reference the badger-accordion object inside out eventListener
      var _this = this; // Adding click event to accordion


      this.triggers.forEach(function (trigger, index) {
        trigger.addEventListener('click', function () {
          // Getting the target of the click
          // const clickedEl = event.target;
          _this.handleClick(trigger, index);
        });
      });
    }
    /**
     *  HANDLE CLICK
     *
     *  Handles click and checks if click was on an trigger element
     *  @param {object} targetTrigger - The trigger node you want to open
     */

  }, {
    key: "handleClick",
    value: function handleClick(targetTrigger, triggerIndex) {
      // Removing current `.` from `this.settings.triggerClass` class so it can
      // be checked against the `targeTrigger` classList
      var targetTriggerClass = this.settings.triggerClass.substr(1); // Checking that the thing that was clicked on was the accordions trigger

      if (targetTrigger.classList.contains(targetTriggerClass) && this.toggling === false) {
        this.toggling = true; // Updating states

        this.setState(triggerIndex); // Render DOM as per the updates `this.states` object

        this._renderDom();
      }
    }
    /**
     *  SET STATES
     *
     *  Sets the state for all triggers. The 'target trigger' will have its state toggeled
     *  @param {object} targetTriggerId - The trigger node you want to open
     */

  }, {
    key: "setState",
    value: function setState(targetTriggerId) {
      var _this3 = this;

      var states = this.getState(); // If `this.settings.openMultiplePanels` is false we need to ensure only one panel
      // be can open at once. If it is false then all panels state APART from the one that
      // has just been clicked needs to be set to 'closed'.

      if (!this.settings.openMultiplePanels) {
        states.filter(function (state, index) {
          if (index != targetTriggerId) {
            state.state = 'closed';
          }
        });
      } // Toggles the state value of the target trigger. This was `array.find` but `find`
      // isnt supported in IE11


      states.filter(function (state, index) {
        if (index == targetTriggerId) {
          var newState = _this3.toggleState(state.state);

          return state.state = newState;
        }
      });
    }
    /**
     *  RENDER DOM
     *
     *  Renders the accordion in the DOM using the `this.states` object
     */

  }, {
    key: "_renderDom",
    value: function _renderDom() {
      var _this4 = this;

      // const states = this.getState();
      // Filter through all open triggers and open them
      this.states.filter(function (state, index) {
        if (state.state === 'open') {
          _this4.open(index);
        }
      }); // Filter through all closed triggers and closes them

      this.states.filter(function (state, index) {
        if (state.state === 'closed') {
          _this4.close(index);
        }
      });
    }
    /**
     *  OPEN
     *
     *  Closes a specific panel
     *  @param {integer} triggerIndex - The trigger node index you want to open
     */

  }, {
    key: "open",
    value: function open(triggerIndex) {
      this.togglePanel('open', triggerIndex);
    }
    /**
     *  CLOSE
     *
     *  Closes a specific panel
     *  @param {integer} triggerIndex - The trigger node index you want to close
     */

  }, {
    key: "close",
    value: function close(triggerIndex) {
      this.togglePanel('closed', triggerIndex);
    }
    /**
     *  OPEN ALL
     *
     *  Opens all panels
     */

  }, {
    key: "openAll",
    value: function openAll() {
      var _this5 = this;

      this.items.forEach(function (item, triggerIndex) {
        _this5.togglePanel('open', triggerIndex);
      });
    }
    /**
     *  CLOSE ALL
     *
     *  Closes all panels
     */

  }, {
    key: "closeAll",
    value: function closeAll() {
      var _this6 = this;

      this.item.forEach(function (item, triggerIndex) {
        _this6.togglePanel('closed', triggerIndex);
      });
    }
    /**
     *  GET STATE
     *
     *  Getting state of triggers. By default gets state of all triggers
     *  @param {string} animationAction - The animation you want to invoke
     *  @param {integer} triggerIndex    - The trigger node index you want to animate
     */

  }, {
    key: "togglePanel",
    value: function togglePanel(animationAction, triggerIndex) {
      var _this7 = this;

      if (animationAction !== undefined && triggerIndex !== undefined) {
        if (animationAction === 'closed') {
          // 1. Getting ID of panel that we want to close
          var trigger = this.triggers[triggerIndex];
          var panelToClose = this.panels[triggerIndex];
          var item = this.items[triggerIndex]; // 2. Closing panel

          item.classList.add(this.settings.hiddenClass); // 3. Removing active classes

          item.classList.remove(this.settings.activeClass); // 4. Set aria attrs

          item.setAttribute('aria-expanded', false);
          trigger.setAttribute('aria-label', this.settings.triggerOpenLabel); // 5. Resetting toggling so a new event can be fired

          panelToClose.onCSSTransitionEnd(function () {
            return _this7.toggling = false;
          });
        } else if (animationAction === 'open') {
          // 1. Getting ID of panel that we want to open
          var _trigger = this.triggers[triggerIndex];
          var panelToOpen = this.panels[triggerIndex];
          var _item = this.items[triggerIndex]; // 2. Opening panel

          _item.classList.remove(this.settings.hiddenClass); // 3. Adding active classes


          _item.classList.add(this.settings.activeClass); // 4. Set aria attrs


          _item.setAttribute('aria-expanded', true);

          _trigger.setAttribute('aria-label', this.settings.triggerCloseLabel); // 5. Resetting toggling so a new event can be fired


          panelToOpen.onCSSTransitionEnd(function () {
            return _this7.toggling = false;
          });
        }
      }
    }
    /**
     *  GET STATE
     *
     *  Getting state of items. By default gets state of all items
     *  @param {array} itemIds - Id/'s of the triggers you want to check
     */

  }, {
    key: "getState",
    value: function getState() {
      var _this8 = this;

      var itemIds = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

      if (itemIds.length && Array.isArray(itemIds)) {
        var states = itemIds.map(function (item) {
          return _this8.states[item];
        });
        return states;
      } else {
        return this.states;
      }
    }
    /**
     *  TOGGLE STATE
     *
     *  Toggling the state value
     *  @param {string} currentState - Current state value for a iten
     */

  }, {
    key: "toggleState",
    value: function toggleState(currentState) {
      if (currentState !== undefined) {
        return currentState === 'closed' ? 'open' : 'closed';
      }
    }
    /**
     *  Items TO OPEN
     *
     *  Setting which item should be open when accordion is initalised
     *  @param {array} itemsToOpen - Array of ID's for the item to be open
     */

  }, {
    key: "_openItemsOnLoad",
    value: function _openItemsOnLoad() {
      var _this9 = this;

      var itemsToOpen = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

      if (itemsToOpen.length && Array.isArray(itemsToOpen)) {
        var items = itemsToOpen.filter(function (item) {
          return item != undefined;
        });
        items.forEach(function (item) {
          return _this9.states[item].state = 'open';
        });
      }
    }
    /**
     *  SET UP ATTRIBUTES
     *
     *  Initalises accordion attribute methods
     */

  }, {
    key: "_setupAttributes",
    value: function _setupAttributes() {
      // Adding ID & aria-controls
      this._setupTriggers(); // Adding ID & aria-labeledby


      this._setupPanels();

      this._setupItems(); // Inserting data-attribute onto each `trigger`


      this._insertDataAttrs();
    }
    /**
     *  CALCULATE PANEL HEIGHT
     *
     *  Setting height for panels using pannels inner element
     */

  }, {
    key: "calculatePanelHeight",
    value: function calculatePanelHeight(panel) {
      var panelInner = panel.querySelector(this.settings.panelInnerClass);
      var activeHeight = panelInner.offsetHeight;
      return panel.style.maxHeight = "".concat(activeHeight, "px");
    }
    /**
     *  CALCULATE PANEL HEIGHT
     *
     *  Setting height for panels using pannels inner element
     */

  }, {
    key: "calculateAllPanelsHeight",
    value: function calculateAllPanelsHeight() {
      var _this10 = this;

      this.panels.forEach(function (panel) {
        _this10.calculatePanelHeight(panel);
      });
    }
    /**
     * SET UP TRIGGERS
     */

  }, {
    key: "_setupTriggers",
    value: function _setupTriggers() {
      var _this11 = this;

      this.triggers.forEach(function (trigger, index) {
        trigger.setAttribute('id', "accordion-trigger-".concat(_this11.ids[index].id));
        trigger.setAttribute('aria-controls', "accordion-panel-".concat(_this11.ids[index].id));
        trigger.setAttribute('aria-label', _this11.settings.triggerOpenLabel);
      });
    }
    /**
     * SET UP PANELS
     */

  }, {
    key: "_setupPanels",
    value: function _setupPanels() {
      var _this12 = this;

      this.panels.forEach(function (panel, index) {
        panel.setAttribute('id', "accordion-panel-".concat(_this12.ids[index].id));
        panel.setAttribute('aria-labeledby', "accordion-trigger-".concat(_this12.ids[index].id));

        if (_this12.settings.roles === true || _this12.settings.roles.region !== false) {
          _this12._setRole('region', panel);
        }
      });
    }
    /**
     * SET UP ITEMS
     */

  }, {
    key: "_setupItems",
    value: function _setupItems() {
      var _this13 = this;

      this.items.forEach(function (item, index) {
        item.setAttribute('id', "accordion-item-".concat(_this13.ids[index].id));
        item.setAttribute('aria-label', _this13.settings.itemLabel);
      });
    }
  }]);

  return Accordion;
}(); // Export

return Accordion;

})));
//# sourceMappingURL=accordion.min.js.map
